#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int sqr(int num1, int num2){
    int64_t output = num1;
    while (num2 > 1) {
        output *= num1;
        num2 -=1;
    }
    return output;
}
int main(int argc, char *argv[]){
    if(argc != 4){
        printf("not enough vaild outputs %d",argc);
        return 0;
    }
    else {
        int num1 = atoi(argv[1]);
        int num2 = atoi(argv[3]);
        int output = 0;

        if (!strcmp(argv[2], "+")){output = num1 + num2;}
        if (!strcmp(argv[2], "-")){output = num1 - num2;}
        if (!strcmp(argv[2], "/")){output = num1 / num2;}
        if (!strcmp(argv[2], "*")){output = num1 * num2;}
        if (!strcmp(argv[2], "^")){output = sqr(num1, num2);}
        printf("%d\n", output);
        return 0;
    }}
